import { game } from '../stores';
import { questions } from '../stores/questions';

export function resetGame() {
    game.currQuestion.set(0);
}

export function startGame() {
    resetGame();
    game.answers.set([]);
    game.results.set(game.startingResults);
    const shuffled = questions
        .map(value => ({ value, sort: Math.random() }))
        .sort((a, b) => a.sort - b.sort)
        .map(({ value }) => value);
    game.questions.set(shuffled);
}
