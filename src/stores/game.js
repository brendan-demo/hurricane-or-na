import { writable } from "svelte/store";

export const currQuestion = writable(-1);

export const questions = writable([]);

export const answers = writable([]);

export const startingResults = {
    progress: 0,
    correct: 0,
    incorrect: 0,
}

export const results = writable(startingResults);