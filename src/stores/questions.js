export const questions = [
    {
        song: "Hurricane",
        album: "Hamilton",
        hurricane: true,
        line: "In the eye of a hurricane<br/> There is quiet<br/> For just a moment<br/> A yellow sky"
    },
    {
        song: "Alexander Hamilton",
        album: "Hamilton",
        hurricane: true,
        line: "Then a hurricane came, and devastation reigned<br/> Our man saw his future drip, dripping down the drain"
    },
    {
        song: "My Shot",
        album: "Hamilton",
        hurricane: false
    },
    {
        song: "Stay Alive",
        album: "Hamilton",
        hurricane: false
    },
    {
        song: "Non-Stop",
        album: "Hamilton",
        hurricane: false
    },
    {
        song: "We Don't Talk About Bruno",
        album: "Encanto",
        hurricane: true,
        line: "Married in a hurricane"
    },
    {
        song: "What Else Can I Do?",
        album: "Encanto",
        hurricane: true,
        line: "A hurricane of jacarandas<br/> Strangling fig, hanging vines"
    },
    {
        song: "The Family Madrigal",
        album: "Encanto",
        hurricane: false
    },
    {
        song: "Dos Oruguitas",
        album: "Encanto",
        hurricane: false
    },
    {
        song: "All of You",
        album: "Encanto",
        hurricane: false
    },
]